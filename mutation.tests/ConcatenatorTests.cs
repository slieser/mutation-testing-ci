using NUnit.Framework;

namespace mutation.tests;

[TestFixture]
public class ConcatenatorTests
{
    [Test]
    public void Concat_works_as_expected() {
        var result = Concatenator.Concat("aaa");
        Assert.That(result, Is.EqualTo("aaabbb"));
    }
}