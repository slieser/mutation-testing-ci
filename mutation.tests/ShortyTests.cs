using NUnit.Framework;

namespace mutation.tests
{
    public class ShortyTests
    {
        [Test]
        public void Shorter_string_is_returned_as_is() {
            Assert.That(Shorty.Shorten("A", 5), Is.EqualTo("A"));
        }

        [Test]
        public void String_of_exact_length_is_returned_as_is() {
            Assert.That(Shorty.Shorten("A", 1), Is.EqualTo("A"));
            Assert.That(Shorty.Shorten("AB", 2), Is.EqualTo("AB"));
            Assert.That(Shorty.Shorten("ABC", 3), Is.EqualTo("ABC"));
        }

        [Test]
        public void Longer_string_contains_Ellipsis() {
            Assert.That(Shorty.Shorten("Stefan Lieser", 9), Is.EqualTo("Ste...ser"));
        }

        [Test]
        public void Ellipsis_is_inserted_if_length_is_greater_6() {
            Assert.That(Shorty.Shorten("1234567890", 7), Is.EqualTo("12...90"));
            Assert.That(Shorty.Shorten("123456789", 7), Is.EqualTo("12...89"));
            Assert.That(Shorty.Shorten("12345678", 7), Is.EqualTo("12...78"));
            Assert.That(Shorty.Shorten("1234567", 7), Is.EqualTo("1234567"));
        }

        [Test]
        public void No_Ellipsis_is_inserted_if_length_is_less_than_7() {
            Assert.That(Shorty.Shorten("1234567890", 6), Is.EqualTo("123456"));
            Assert.That(Shorty.Shorten("1234567890", 5), Is.EqualTo("12345"));
        }

        [Test]
        public void Even_length() {
            // This test catches a mutant when running dotnet stryker
            Assert.That(Shorty.Shorten("1234567890", 8), Is.EqualTo("123...90"));
        }
    }
}